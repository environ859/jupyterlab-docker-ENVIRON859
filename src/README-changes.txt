John,

There are now JupyterLab containers for ENVIRON859. I set things up close to how we have outfitted the
Biostats people - we can change things if you like. 

Each user has a private persistent home directory, all the students have read-only access 
to a shared data directory at /data. The instructor (you) has read/write access to /data.
Everyone (students and instructor) has access to a shared_data directory named /shared_space.

To get to the JupyterLab instances go to 
     https://vm-manage.oit.duke.edu/containers
and click on the link to log into “ ENVIRON859 - Advanced Geospatial Analysis”.

The Docker container source is in this gitlab project:
     https://gitlab.oit.duke.edu/environ859/jupyterlab-docker-ENVIRON859

Commits made to that staging branch of the project will trigger a build and deploy to the test
instance which you can access by going to
     https://ipyn-az-19.oit.duke.edu/
and using the password
     this_is_the_test_password
to log in.

You can see the CI build/deploy pipelines for the project here
     https://gitlab.oit.duke.edu/environ859/jupyterlab-docker-ENVIRON859/pipelines

From the CI/CD pipeline page click on the “passed” or “failed” button to get to the detail 
of a given continuous integration run.

Once you are on the pipeline detail page, click on the green checkbox or red failed X next to a stage
to see a transcript of what happened during that stage.

After you are satisfied with the changes made in staging, you should commit your changes and push them
to the master branch of the project. This will trigger a build of the production container image. I’ve set the 
CI script to require that you click on the deploy button so that a commit to the master branch doesn’t 
accidentally trigger a restart everyone’s container at an inopportune time.

The button to click to trigger a deploy to production (and restart/replace everyone’s container instances)
can be found if you click on the green “passed” button shown for that pipeline to get to the pipeline detail
page, then click on the “deploy production” button.

For now, that containers are running in the Azure cloud and I guessed at sizing the server.
It would be idea to have a worst-case job to run to help us tune the server sizing and set cgroup
memory limits on the student containers.
